from django.db import models
from django.utils.translation import gettext as _
from django.contrib.auth.password_validation import validate_password, password_validators_help_texts
from django.core.validators import validate_email
from django.contrib.auth.hashers import make_password
from django.contrib.auth import get_user_model
from django.contrib.auth.models import BaseUserManager
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.conf import settings

from simple_email_confirmation.models import SimpleEmailConfirmationUserMixin

from user.intermediateprocessing import make_html_email


class UserManager(BaseUserManager):
    use_in_migrations = True

    def email_not_exist(self, email):
        try:
            get_user_model().objects.get(email = email)
            return False
        except:
            return True

    def password_is_valid(self, password):
        try:
            if validate_password(password) == None:
                return True
            return False
        except:
            return False

    def email_is_valid(self, email):
        try:
            if validate_email(email) == None:
                return True
            return False
        except:
            return False

    def _create_user(self, email, password, **kwargs):
        if not email:
            raise ValidationError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **kwargs):
        kwargs.setdefault('is_staff', False)
        kwargs.setdefault('is_superuser', False)
        if self.email_is_valid(email):
            if self.password_is_valid(password):
                if self.email_not_exist(email):
                    return self._create_user(email, password, **kwargs)
                raise ValidationError('Email is not available')
            raise ValidationError('Password is not valid')
        raise ValidationError('Email is not valid')

    def create_superuser(self, email, password, **kwargs):
        kwargs.setdefault('is_staff', True)
        kwargs.setdefault('is_superuser', True)
        if kwargs.get('is_staff') is not True:
            raise ValidationError('Superuser must have is_staff=True.')
        if kwargs.get('is_superuser') is not True:
            raise ValidationError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **kwargs)

    def remove_user(self, user):
        return user.delete()



class UserResponsiveManager(UserManager):

    def get_user_by_email(self, email):
        try:
            return {'success' : True, 'objects' : get_user_model().objects.get(email = email)}
        except:
            return {'success' : False, 'message' : _('User not found')}


    def create_user(self, email, password, **kwargs):
        try:
            user = super(UserResponsiveManager, self).create_user(email, password, **kwargs)
            return {'success' : True, 'objects' : user}
        except:
            if self.email_is_valid(email):
                if self.password_is_valid(password):
                    if not self.email_not_exist(email):
                        return {'success' : False, 'message' : _('Email is not available')}
                    return {'success' : False, 'message' : _('Something went wrong, please check password and email and try again')}
                return {'success' : False, 'message' : _('Password is not valid')}
            return {'success' : False, 'message' : _('Email is not valid')}
                

    def change_email(self, email, user):
        if self.email_not_exist(email):
            if self.email_is_valid(email):
                user.email = email
                user.save()
                user.add_email_if_not_exists(user.email)
                return {'success' : True, 'objects' : user}
            return {'success' : False, 'message' : _('Email is not valid')}
        return {'success' : False, 'message' : _('Email is not available')}


    def change_password(self, password, user):
        try:
            if validate_password(password, user = user) == None:
                user.set_password(password)
                user.save()    
                return {'success' : True, 'message' : 'Password success changed'}
            return {'success' : False, 'message' : 'Something went wrong'}
        except:
            password_validators_help_texts_ = ' '.join(str(s) for s in password_validators_help_texts())
            return {'success' : False, 'message' : password_validators_help_texts_}


    def remove_user(self, user):
        try:
            return {'success' : True, 'objects' : super(UserResponsiveManager, self).remove_user(user)}
        except:
            return {'success' : False, 'message' : 'Something went wrong'}