from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.template.loader import get_template
from django.shortcuts import redirect, reverse
from django.conf import settings

def make_html_email(subject, template, dict_context, from_, to):
    content = render_to_string(template, dict_context)
    email = EmailMultiAlternatives(subject, '', from_, [to],)
    email.attach_alternative(content, 'text/html')
    return email


def login_required(redirect_to):
    def wrap(function):
        def wrapped(request, *args, **kwargs):
            if request.user.is_authenticated:
                return function(request, *args, **kwargs)
            if redirect_to:
                return redirect(redirect_to)
            return redirect('/login')
        return wrapped
    return wrap

def not_login(redirect_to):
    def wrap(function):
        def wrapped(request, *args, **kwargs):
            if not request.user.is_authenticated:
                return function(request, *args, **kwargs)
            if redirect_to:
                return redirect(redirect_to)
            return redirect('/account')
        return wrapped
    return wrap

