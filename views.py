from django.shortcuts import render, redirect, reverse
from django.views.generic.base import View
from django.utils.translation import get_language
from django.contrib import messages
from django.utils.translation import gettext as _
from django.contrib.auth import get_user_model
from django.contrib.auth import logout, login, authenticate
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin

from user.forms import CaptchaForm, SignUpForm, LogInForm, EmailForm, PasswordForm

from user.intermediateprocessing import make_html_email, not_login




class SignUp(View):
    template_name = 'user/signup.html'

    @method_decorator(not_login('/main'))
    def get(self, request, *args, **kwargs):
        form = SignUpForm()
        captcha = CaptchaForm(language = get_language(), data_size = 'normal')
        return render(request, self.template_name, {'form' : form, 'captcha' : captcha})
    
    def post(self, request, *args, **kwargs):
        form = SignUpForm(request.POST)
#        captcha = CaptchaForm(request.POST, language = get_language(), data_size = 'normal')
        if form.is_valid():#captcha.is_valid():
            user = get_user_model().responsive.create_user(
                form.cleaned_data['email'], form.cleaned_data['password'])
            if user['success']:
                messages.success(request, _('Account Was Success Created, Please Confirm Your Email'))
                return redirect(reverse('user:sendconfirmemail', kwargs = {'email' : user['objects'].email}))
            messages.error(request, user['message'])
            form = SignUpForm(initial = {'email' : request.POST['email']})
            captcha = CaptchaForm(language = get_language(), data_size = 'normal')
            return render(request, self.template_name, {'form' : form, 'captcha' : captcha})
        messages.error(request, "Form Is Not Valid")
        form = SignUpForm(initial = {'email' : request.POST['email']})
        captcha = CaptchaForm(language = get_language(), data_size = 'normal')
        return render(request, self.template_name, {'form' : form, 'captcha' : captcha})


class ChangeEmail(LoginRequiredMixin, View):#test required
    template_name = 'user/change_email.html'

    def get(self, request, *args, **kwargs):
        form = EmailForm()
        return render(request, self.template_name, {'form' : form})

    def post(self, request, *args, **kwargs):
        form = EmailForm(request.POST)
        if form.is_valid():
            user = get_user_model().responsive.change_email(form.cleaned_data['email'], request.user)
            if user['success']:
                return redirect(reverse('user:sendconfirmemail', kwargs = {'email' : user['objects'].email}))
            messages.error(request, user['message'])
            return redirect('/main')
        messages.error(request, 'Form Is Not Valid')
        form = EmailForm(initial = {'email' : request.POST['email']})
        return render(request, self.template_name, {'form' : form})


class ChangePassword(LoginRequiredMixin, View):#test required
    template_name = 'user/change_password.html'

    def get(self, request, *args, **kwargs):
        form = PasswordForm()
        return render(request, self.template_name, {'form' : form})

    def post(self, request, *args, **kwargs):
        form = PasswordForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            change_password = get_user_model().responsive.change_password(form.cleaned_data['password'], request.user)
            if change_password['success']:
                messages.info(request, change_password['message'])
                return redirect('/main')
            messages.error(request, change_password['message'])
            return redirect('/main')
        messages.error(request, 'Form Is Not Valid')
        form = PasswordForm(initial = {'password' : request.POST['password']})
        return render(request, self.template_name, {'form' : form})



def sendConfirmEmail(request, *args, **kwargs):
    if request.method == 'GET':
        user = get_user_model().responsive.get_user_by_email(kwargs['email'])
        if user['success']:
            if not user['objects'].is_confirmed:
                confirmation_email = make_html_email(_('Confirm Email (noreply)'), 'user/confirm_email_emailtemplate.html', 
                    {'id' : user['objects'].id, 'confirmation_link' : request.META.get('HTTP_HOST') + reverse('user:confirmemail',
                         kwargs = {'id' : user['objects'].id, 'confirmation_key' : user['objects'].confirmation_key})}, 
                    settings.SITE_NAME, user['objects'].email)
                confirmation_email.send()
                messages.info(request, 'Confirmation Email Has Success Sent, Please Check Mailbox')
                return redirect(reverse('user:login'))
            messages.error(request, 'Your Email Has Already Been Confirmed')
            return redirect(request.META.get('HTTP_REFERER'))    
        messages.error(request, user['message'])
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(reverse('404'))



def confirmEmail(request, *args, **kwargs):
    if request.method == 'GET':
        try:
            user = get_user_model().objects.get(id = kwargs['id'])
            user.confirm_email(kwargs['confirmation_key'])
            if user.email in user.confirmed_emails:
                messages.success(request, 'Your Email Was Success Confirmed')
                return redirect('/main')
            messages.error(request, 'Something Went Wrong')
            return redirect(reverse('user:login'))
        except:
            messages.error(request, 'Something Went Wrong. Please Try Again')
        return redirect('/main')
    return redirect(reverse('404'))


@login_required
def removeUser(request, *args, **kwargs):
    if request.method == 'GET':
        remove = get_user_model().responsive.remove_user(request.user)
        if remove['success']:
            messages.info(request, 'User Has Been Success Removed')
            return redirect('/main')
        messages.error(request, remove['message'])
        return redirect('/main')
    return redirect(reverse('404'))