from django import forms
from django.utils.translation import gettext as _

from captcha.fields import ReCaptchaField, ReCaptchaV2Checkbox


class CaptchaForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.language = kwargs.pop('language')
        self.data_size = kwargs.pop('data_size')
        super(CaptchaForm, self).__init__(*args, **kwargs)
        self.fields['captcha'] = ReCaptchaField(widget = ReCaptchaV2Checkbox(api_params={'hl': self.language}, attrs={'data-size': self.data_size}))


class EmailForm(forms.Form):
    email = forms.EmailField(label = _('Email'), 
        widget = forms.EmailInput(attrs = {
            "class" : "form-control", 
            "id" : "email", 
            "placeholder" : _("Email")
            })
        )

class PasswordForm(forms.Form):
    password = forms.CharField(label = _('Password'), 
        widget = forms.PasswordInput(attrs = {   
            "class" : "form-control",
            "id" : "password",
            "placeholder" : _("Password")
            })
        )

class FullNameForm(forms.Form):
    first_name = forms.CharField(max_length = 150, required = False, label = ('First Name'), widget = forms.TextInput(attrs = {
            "class" : "form-control",
            "id" : "first_name",
            "placeholder" : _("First Name (optional)")
            })
        )
    last_name = forms.CharField(max_length = 150, required = False, label = ('Last Name'), widget = forms.TextInput(attrs = {
            "class" : "form-control",
            "id" : "last_name",
            "placeholder" : _("Last Name (optional)")
            })
        )


class SignUpForm(EmailForm, PasswordForm):
    pass


class LogInForm(EmailForm, PasswordForm):
    pass
