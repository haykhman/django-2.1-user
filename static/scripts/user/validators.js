function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    }

function checkEmail(){
    var email = $('#email').val();
    if (validateEmail(email)){
        return true
    }
    else{
        return false
    }
}
    
function checkPassword(){
    var password = $('#password').val();
    if(password.length >= 8){
        return true
    }
    else{
        return false
    }
}