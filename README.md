#User

## Required Packages

Django==2.1

Python 3.6.7

django-simple-email-confirmation

django-recaptcha

bootstrap 4.3.1

JQuery 3.3.1

popper

for tests correct working require pytest

## settings.py 

#### INSTALLED_APPS 
 'simple_email_confirmation', 
 'captcha', 

 'user', 

 'user.resetpassword', 
 
#### .
 STATICFILES_DIRS = [..., os.path.join(BASE_DIR, 'user/static')] 
 
#### .
AUTH_USER_MODEL = 'user.User' 
 
#### .
LANGUAGES = (['en', 'English'],) 
 
#### email will show this name 
SITE_NAME = 'site name' 
 
#### .
RESETPASSWORD_EXPIRE_HOURS = 3 

RESETPASSWORD_URLSAFE_LENGTH = 32 
 
#### require for correct login and logout 
LOGIN_URL = '/user/login' 

LOGIN_REDIRECT_URL = '/main' 

LOGOUT_REDIRECT_URL = '/main' 
 
#### keys get from https://www.google.com/recaptcha 
RECAPTCHA_PRIVATE_KEY = 'SOMELONGKEY'

RECAPTCHA_PUBLIC_KEY = 'SOMELONGKEY'
 
#### for correct send email 
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'

EMAIL_USE_TLS = True

EMAIL_PORT = 587

EMAIL_HOST_USER = 'example@gmail.com'

EMAIL_HOST_PASSWORD = 'somepassword'

#### .
Add include with name 'user'. 
**Example** 
```python
    re_path('user/', include(('user.urls.urls_user', 'user'), namespace = 'user'))
``` 

#### .
User application use 'main' named direction (after success transactions it redirect to main) 

Applicatiin use built in LogoutView and LoginView 

For correct migration for first make "user" migration

#### Static
For correct display templates add (manually) static packages, like the following tree

```sh
 static
│   ├── bootstrap
│   │   └── bootstrap-4.3.1-dist
│   │       ├── css
│   │       │   ├── bootstrap.css
│   │       │   ├── ...
│   │       │
│   │       └── js
│   │           ├── bootstrap.js
│   │           ├── ...
│   ├── jquery
│   │   └── jquery-3.3.1.min.js
│   └── popper.min.js
```