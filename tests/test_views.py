from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from django.utils.translation import get_language
from django.contrib.auth import authenticate, login

from user.forms import SignUpForm, LogInForm, EmailForm, PasswordForm, CaptchaForm


class ViewTests(TestCase):
    def __init__(self, *args, **kwargs):
        self.client = Client()
        super(ViewTests, self).__init__(*args, **kwargs)

    def setUp(self):
        get_user_model().objects.create(email = 'hmyan@yandex.com', password = 'hmpassword').save()

    def tearDown(self):
        get_user_model().objects.get(email = 'hmyan@yandex.com').delete()

    def test_signup_get(self):
        form = SignUpForm()
        captcha = CaptchaForm(language = get_language(), data_size = 'normal')
        valid_get = self.client.get(reverse('user:signup'), {'form' : form, 'captcha' : captcha})
        self.assertEquals(valid_get.status_code, 200)

    def test_signup_post(self):
        self.client.post(reverse('user:signup'), {'email' : 'hmyan@yandex.ru', 'password' : 'somepassword'})
        user = get_user_model().objects.get(email = 'hmyan@yandex.ru')
        self.assertEqual(user.email, 'hmyan@yandex.ru')

    #test required ChangeEmail, ChangePassword, confirmEmail, sendConfirmEmail