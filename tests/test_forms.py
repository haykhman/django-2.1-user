from django.test import TestCase

from user.forms import EmailForm, PasswordForm, FullNameForm, SignUpForm, LogInForm

class TestForms(TestCase):

    def test_email_form_fields(self):
        form = EmailForm()
        self.assertEqual(form.fields['email'].label, 'Email')
        self.assertEqual(form.fields['email'].widget.attrs, {"class" : "form-control", "id" : "email", "placeholder" : "Email"})

    def test_password_form_fields(self):
        form = PasswordForm()
        self.assertEqual(form.fields['password'].label, 'Password')
        self.assertEqual(form.fields['password'].widget.attrs, {"class" : "form-control", "id" : "password", "placeholder" : "Password"})


    def test_fullname_form_fields(self):
        form = FullNameForm()
        self.assertEqual(form.fields['first_name'].label, 'First Name')
        self.assertEqual(form.fields['first_name'].required, False)
        self.assertEqual(form.fields['first_name'].widget.attrs, {"class" : "form-control", "id" : "first_name", "placeholder" : "First Name (optional)", "maxlength" : "150"})
        self.assertEqual(form.fields['last_name'].label, 'Last Name')
        self.assertEqual(form.fields['last_name'].required, False)
        self.assertEqual(form.fields['last_name'].widget.attrs, {"class" : "form-control", "id" : "last_name", "placeholder" : "Last Name (optional)", "maxlength" : "150"})
        self.assertEqual(len(form.fields), 2) 

    def test_signup_form_fields(self):
        form = SignUpForm()
        self.assertTrue(form.fields['email'])
        self.assertTrue(form.fields['password'])
        self.assertEqual(len(form.fields), 2)

    def test_login_form_fields(self):
        form = LogInForm()
        self.assertTrue(form.fields['email'])
        self.assertTrue(form.fields['password'])
        self.assertEqual(len(form.fields), 2)
