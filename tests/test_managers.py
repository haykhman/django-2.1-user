from django.test import TestCase

from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
import pytest

class UserManagerTest(TestCase):
    exist_email = 'hmyan@yandex.com'
    not_exist_email = 'none@none.none'
    not_valid_email = 'none@none'
    valid_password = 'somepswr' #symbols length 8
    not_valid_password = 'somepsw'

    def setUp(self):
        get_user_model().objects.create(email = self.exist_email, password = self.valid_password).save()

    def tearDown(self):
        get_user_model().objects.get(email = self.exist_email).delete()
    
    def test_email_not_exist(self):
        check_exist_email = get_user_model().objects.email_not_exist(self.exist_email)
        chack_not_exist_email = get_user_model().objects.email_not_exist(self.not_exist_email)
        self.assertFalse(check_exist_email)
        self.assertTrue(chack_not_exist_email)

    def test_email_is_valid(self):
        valid_email = get_user_model().objects.email_is_valid(self.not_exist_email)
        not_valid_email = get_user_model().objects.email_is_valid(self.not_valid_email)
        self.assertTrue(valid_email)
        self.assertFalse(not_valid_email)
    
    def test_password_is_valid(self):
        valid_passsword = get_user_model().objects.password_is_valid(self.valid_password)
        not_valid_password = get_user_model().objects.password_is_valid(self.not_valid_password)
        self.assertTrue(valid_passsword)
        self.assertFalse(not_valid_password)

    def test_create_user_email(self):
        with pytest.raises(ValidationError):
            get_user_model().objects.create_user(self.exist_email, self.valid_password)
        with pytest.raises(ValidationError):
            get_user_model().objects.create_user(self.not_valid_email, self.valid_password)
        valid_user = get_user_model().objects.create_user(self.not_exist_email, self.valid_password)
        get_valid_user = get_user_model().objects.get(email = self.not_exist_email)
        self.assertEqual(valid_user, get_valid_user)

    def test_create_user_password(self):
        with pytest.raises(ValidationError):
            get_user_model().objects.create_user(self.not_exist_email, self.not_valid_password)
        validation_valid_password_user = get_user_model().objects.create_user(self.not_exist_email, self.valid_password)
        get_valid_user = get_user_model().objects.get(email = self.not_exist_email)
        self.assertEqual(validation_valid_password_user, get_valid_user)

    #--- User Responsive Manager Tests ---
    def test_get_user_by_email(self):
        existed_user = get_user_model().responsive.get_user_by_email(self.exist_email)
        not_existed_user = get_user_model().responsive.get_user_by_email(self.not_exist_email)
        self.assertTrue(existed_user['success'])
        self.assertFalse(not_existed_user['success'])

    def test_create_user(self):
        valid_create_user = get_user_model().responsive.create_user(self.not_exist_email, self.valid_password)
        existed_create_user = get_user_model().responsive.create_user(self.exist_email, self.valid_password)
        not_valid_email_create_user = get_user_model().responsive.create_user(self.not_valid_email, self.valid_password)
        not_valid_password_create_user = get_user_model().responsive.create_user(self.not_exist_email, self.not_valid_password)
        self.assertTrue(valid_create_user['success'])
        self.assertFalse(existed_create_user['success'])
        self.assertFalse(not_valid_email_create_user['success'])
        self.assertFalse(not_valid_password_create_user['success'])

    def test_change_email(self):
        user = get_user_model().objects.get(email = self.exist_email)
        changable_user = get_user_model().objects.create_user(self.not_exist_email, self.valid_password)
        not_valid_change_email = get_user_model().responsive.change_email(self.not_valid_email, changable_user)
        valid_change_email = get_user_model().responsive.change_email('some@email.com', changable_user)
        existed_chane_email = get_user_model().responsive.change_email(user.email, user)
        self.assertTrue(valid_change_email['success'])
        self.assertFalse(not_valid_change_email['success'])
        self.assertFalse(existed_chane_email['success'])



        