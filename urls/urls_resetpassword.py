from django.urls import re_path, path

from user.resetpassword.views import SendResetLinkToEmail, ChangePassword, removeReset

urlpatterns = [
    path('reset/', SendResetLinkToEmail.as_view(), name = 'reset'),
    path('change/<str:path>', ChangePassword.as_view(), name = 'change'),
    path('remove/<str:path>', removeReset, name = 'remove')
]