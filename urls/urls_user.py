from django.urls import re_path, path, include
from django.contrib.auth.views import LogoutView, LoginView

from user.views import SignUp, confirmEmail, sendConfirmEmail, ChangeEmail, ChangePassword, removeUser

from django.conf import settings

import os

user_dir = os.path.basename(os.path.normpath(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))


urlpatterns = [
    path('signup/', SignUp.as_view(), name = 'signup'),
    path('login/', LoginView.as_view(template_name = 'user/login.html'), name = 'login'),
    path('logout/', LogoutView.as_view(), name = 'logout'),
    path('removeuser/', removeUser, name = 'removeuser'),
    path('confirmemail/<str:id>/<str:confirmation_key>/', confirmEmail, name = 'confirmemail'),
    path('sendconfirmemail/<str:email>', sendConfirmEmail, name = 'sendconfirmemail'),
    path('change/email', ChangeEmail.as_view(), name = 'change_email'),
    path('change/password', ChangePassword.as_view(), name = 'change_password'),

    re_path(r'^resetpassword/', include((user_dir + '.urls.urls_resetpassword', 'resetpassword'), namespace = 'resetpassword')),
]
