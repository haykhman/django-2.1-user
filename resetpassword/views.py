from django.shortcuts import render, redirect, HttpResponse, HttpResponsePermanentRedirect, reverse
from django.views.generic.base import View, TemplateView
from django.core.mail import send_mail
from django.utils.translation import gettext as _
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.utils.translation import get_language
from user.models import UserManager
from django.conf import settings

from user.resetpassword.models import ResetPassword
from user.resetpassword.forms import ResetEmailField, ResetPasswordField
from user.intermediateprocessing import make_html_email
from user.forms import CaptchaForm




class SendResetLinkToEmail(View):
    template_name = 'user/resetpassword/reset_link_to_email.html'

    def get(self, request, *args, **kwargs):
        form = ResetEmailField()
        self.captcha = CaptchaForm(language = get_language(), data_size = 'normal')
        return render(request, self.template_name, {'form' : form, 'captcha' : self.captcha})

    def post(self, request, *args, **kwargs):
        form = ResetEmailField(request.POST)
#        captcha = CaptchaForm(request.POST, language = get_language(), data_size = 'normal')
        if form.is_valid():# and captcha.is_valid():
            user_response = get_user_model().responsive.get_user_by_email(form.cleaned_data['email'])
            if user_response['success']:
                user = user_response['objects']
                reset = ResetPassword.objects.make_reset(user)
                make_email = make_html_email(_('Reset Password (noreply)'), 'user/resetpassword/reset_password_emailtemplate.html',
                 {'security_key' : request.META.get('HTTP_HOST') + reverse('user:resetpassword:change', 
                  kwargs = {'path' : reset.path})}, settings.SITE_NAME, user.email)
                make_email.send()
                messages.success(request, 'A Link To Reset Your Password Has Been Sent To Your Email Address')     
                return redirect('/main')
            messages.warning(request, user_response['message'])
            form = ResetEmailField(initial = {'email' : request.POST['email']})
            return render(request, self.template_name, {'form' : form})
        messages.error(request, 'Form is not valid')
        form = ResetEmailField(initial = {'email' : request.POST['email']})
        return render(request, self.template_name, {'form' : form, 'captcha' : self.captcha})



class ChangePassword(View):
    template_name = 'user/resetpassword/change_password.html'

    def get(self, request, *args, **kwargs):
        resposive_resetpassword_model = ResetPassword.responsive.get_reset_by_path(kwargs['path'])
        if resposive_resetpassword_model['success']:
            form = ResetPasswordField()
            return render(request, self.template_name, {'form' : form, 'security_path' : kwargs['path']})
        messages.error(request, resposive_resetpassword_model['message'])
        return redirect('/main')

    def post(self, request, *args, **kwargs):
        responsive_resetpassword_model = ResetPassword.responsive.get_reset_by_path(kwargs['path'])
        if responsive_resetpassword_model['success']:
            form = ResetPasswordField(request.POST)
            if form.is_valid():
                change_password = get_user_model().responsive.change_password(form.cleaned_data['password'], responsive_resetpassword_model['objects'].user)
                if change_password['success']:
                    messages.success(request, change_password['message'])
                    return redirect(reverse('user:resetpassword:remove', kwargs = {'path' : kwargs['path']}), )
                messages.info(request, change_password['message'])
                return redirect(request.META.get('HTTP_REFERER'))
            messages.error(request, 'Password is not valid')
            form = ResetEmailField(initial = {'password' : request.POST['password']})
            return render(request, self.template_name, {'form' : form})
        messages.error(request, responsive_resetpassword_model['message'])
        return redirect(request.META.get('HTTP_REFERER'))


            
def removeReset(request, *args, **kwargs):
    if request.method == "GET":
        try:
            user = ResetPassword.objects.get_resetpassword_by_path(kwargs['path']).user
            ResetPassword.objects.remove_reset(user)
        except:
            messages.error(request, 'Something Went Wrong')
    return redirect(reverse('user:login'))
