from django.test import TestCase
from django.contrib.auth import get_user_model

from user.resetpassword.models import ResetPassword

class ResetPasswordManagerTests(TestCase):
    
    def test_create_reset(self):
        user = get_user_model().objects.create(email = 'hmyan@yandex.ru', password = 'somepassword')
        create_reset = ResetPassword.objects.create_reset(user)
        get_reset = ResetPassword.objects.get(user = user)
        self.assertAlmostEqual(create_reset, get_reset)

    def test_re_create_reset(self):
        user = get_user_model().objects.create(email = 'hmyan@yandex.ru', password = 'somepassword')
        create_reset = ResetPassword.objects.create_reset(user)
        recreate_reset = ResetPassword.objects.recreate_reset(user)
        self.assertNotEqual(create_reset.path, recreate_reset.path)

    def test_get_resetpassword_by_path(self):
        user = get_user_model().objects.create(email = 'hmyan@yandex.ru', password = 'somepassword')
        create_reset = ResetPassword.objects.create_reset(user)
        get_by_path = ResetPassword.objects.get_resetpassword_by_path(create_reset.path)
        self.assertEqual(create_reset.path, get_by_path.path)

    def test_get_reset_by_path(self):
        user = get_user_model().objects.create(email = 'hmyan@yandex.ru', password = 'somepassword')
        create_reset = ResetPassword.objects.create_reset(user)
        get_by_path = ResetPassword.responsive.get_reset_by_path(create_reset.path)
        self.assertTrue(get_by_path['success'])

