from django.db import models
from django.utils.translation import gettext as _
from django.utils import timezone
from django.db import models
from django.conf import settings

from datetime import timedelta
import secrets


class ResetPasswordManager(models.Manager):

    def _get_user_resetpassword(self, user):
        return self.get(user = user)

    def _generate_path(self):
        return secrets.token_urlsafe(settings.RESETPASSWORD_URLSAFE_LENGTH)

    def _expire_datetime(self):
        return timezone.now() + timedelta(hours = settings.RESETPASSWORD_EXPIRE_HOURS)

    def create_reset(self, user):
        reset = self.model(user = user, path = self._generate_path(), expire_at = self._expire_datetime())
        reset.save(using = self._db)
        return reset

    def recreate_reset(self, user):
        '''Couse error when ResetPassword model not exist for the user.
           Recomended to use with try except'''
        reset_password = self._get_user_resetpassword(user)
        if reset_password:
            reset_password.delete()
            return self.create_reset(user)
        raise ValueError('resetpassword > Models > ResetPasswordManager > recreate_reset - model not exist')
        
    def make_reset(self, user):
        try:
            return self.create_reset(user)
        except:
            return self.recreate_reset(user)

    def remove_reset(self, user):
        return self._get_user_resetpassword(user).delete()

    def get_resetpassword_by_path(self, path):
        reset_password = self.get(path = path)
        datetimeformat  = '%Y%m%d%H%M%S'
        if reset_password.expire_at.strftime(datetimeformat) > reset_password.created_at.strftime(datetimeformat):
            return reset_password
        raise Exception('user > resetpassword > models > ResetPasswordManager > get_resetpassword_by_path - object is out of date')




class ResetPasswordResponsiveManager(ResetPasswordManager):

    def get_reset_by_path(self, path):
        try:
            return {'success' : True, 'objects' : self.get_resetpassword_by_path(path)}
        except:
            return {'success' : False, 'message' : _('Not Found')}


