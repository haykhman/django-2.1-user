from django.db import models
from user.resetpassword.managers import ResetPasswordManager, ResetPasswordResponsiveManager

from django.contrib.auth import get_user_model


class ResetPassword(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete = models.CASCADE)
    path = models.CharField(max_length = 50)
    created_at = models.DateTimeField(auto_now = True)
    expire_at = models.DateTimeField()

    objects = ResetPasswordManager()
    responsive = ResetPasswordResponsiveManager()
    