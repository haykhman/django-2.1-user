from django import forms
from django.utils.translation import gettext as _
from django.contrib.auth import password_validation
from user.resetpassword.models import ResetPassword

class ResetEmailField(forms.Form):
    email = forms.EmailField(label = _('Email'), widget = forms.EmailInput(attrs = {
            "class" : "form-control",
            "placeholder" : _("Email")
            })
        )


class ResetPasswordField(forms.Form):
    password = forms.CharField(label = _('Password'), widget=forms.PasswordInput(attrs = {   
            "class" : "form-control",
            "id" : "password",
            "placeholder" : _("Password")
            })
        )