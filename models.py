from django.db import models
from django.contrib.auth.models import AbstractUser

from simple_email_confirmation.models import SimpleEmailConfirmationUserMixin

from user.managers import UserManager, UserResponsiveManager


class User(AbstractUser, SimpleEmailConfirmationUserMixin):
    username = None
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    objects = UserManager()
    responsive = UserResponsiveManager()

